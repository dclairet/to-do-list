import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import {BrowserRouter as Router, Link, Route, Routes} from "react-router-dom";
import {Dashboard} from "./components/Dashboard";
import {Todo} from "./components/Todo";

import { CalendarIcon, ChartBarIcon, FolderIcon, HomeIcon, InboxIcon, UsersIcon } from '@heroicons/react/24/outline'
const navigation = [
    { name: 'Tableau de bord', icon: HomeIcon, target: '/', current: false },
    { name: 'Liste des tâches', icon: CalendarIcon, target: '/todo', current: false },
]

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
      <Router>
          <div className="globalContainer grid">
              <div className="flex flex-1 flex-col bg-indigo-700">
                  <div className="flex flex-1 flex-col overflow-y-auto pt-5 pb-4">
                      <div className="flex flex-shrink-0 items-center px-4">
                          <img
                              className="h-8 w-auto"
                              src="calendar.png"
                              alt="ToDoList"
                          />
                      </div>
                      <nav className="mt-5 flex-1 space-y-1 px-2" aria-label="Sidebar">
                          {navigation.map((item) => (
                              <Link to={item.target} className="text-indigo-100 hover:bg-indigo-600 hover:bg-opacity-75 group flex items-center px-2 py-2 text-sm font-medium rounded-md">
                                  <item.icon className="mr-3 h-6 w-6 flex-shrink-0 text-indigo-300" aria-hidden="true" />
                                  <span className="flex-1">{item.name}</span>
                              </Link>
                          ))}
                      </nav>
                  </div>
              </div>

              <Routes>
                  <Route element={<Dashboard />} path="/" />
                  <Route element={<Todo />} path="/todo" />
              </Routes>
          </div>
      </Router>
  </React.StrictMode>
);
