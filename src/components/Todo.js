import {useEffect, useState} from "react";
import {TodoForm} from "./Todoform";
import {TodoList} from "./Todolist";
import {TodoSearch} from "./TodoSearch";

export const Todo = () => {
    const [todoList, setTodoList] = useState(JSON.parse(localStorage.getItem("todoList")) || []);

    useEffect(() => {
        if (todoList?.length > 0) {
            // Check if the deadline is passed
            todoList.forEach((task, index) => {
                if (new Date(task.deadline).setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0)) {
                    const newTaskList = todoList.map((task, i) => {
                        if (i === index) {
                            task.is_passed = true;
                        }
                        return task;
                    });
                }
            });

            localStorage.setItem("todoList", JSON.stringify(todoList));
        }
    }, [todoList]);

    return (
        <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8 mt-10">
            <div className="mx-auto max-w-5xl">
                <h3 className="text-lg font-medium leading-6 text-gray-900">To Do</h3>
                <TodoSearch todoList={todoList} setTodoList={setTodoList} />
                <div className="flex justify-between items-center border-b border-gray-200 py-2"></div>
                <TodoForm todoList={ todoList } setTodoList={ setTodoList } />
                <TodoList todoList={ todoList } setTodoList={ setTodoList } />
            </div>
        </div>
    );
}