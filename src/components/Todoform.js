export const TodoForm = (props) => {
    let date = new Date();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let today = date.getFullYear() + '-' + (month > 10 ? month : '0' + month ) + '-' + (day > 10 ? day : '0' + day);

    // Ajout d'une tâche
    const addTask = (e) => {
        e.preventDefault();
        const newTask = {
            name: e.target.name.value,
            deadline: e.target.deadline.value,
            is_completed: false,
            is_passed: false
        }

        props.setTodoList([...props.todoList, newTask]);
    }

    return (
        <div>
            <form className="flex justify-between mt-8" onSubmit={ addTask }>
                <div
                    className="w-full mr-2 relative rounded-md border border-gray-300 px-3 py-2 shadow-sm focus-within:border-indigo-600 focus-within:ring-1 focus-within:ring-indigo-600">
                    <label htmlFor="name"
                           className="absolute -top-2 left-2 -mt-px inline-block bg-white px-1 text-xs font-medium text-gray-900">Ajouter une tâche</label>
                    <input type="text" name="name"
                           className="block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                           placeholder="Faire à manger..." required/>
                </div>
                <div
                    className="w-full mr-2 relative rounded-md border border-gray-300 px-3 py-2 shadow-sm focus-within:border-indigo-600 focus-within:ring-1 focus-within:ring-indigo-600">
                    <label htmlFor="name"
                           className="absolute -top-2 left-2 -mt-px inline-block bg-white px-1 text-xs font-medium text-gray-900">Deadline</label>
                    <input type="date" min={today} defaultValue={today} name="deadline"
                           className="block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                           required/>
                </div>
                <button className="inline-flex items-center rounded-md border border-transparent bg-indigo-600 px-3 py-2 text-sm font-medium leading-4 text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                    Ajouter
                </button>
            </form>
            <div className="flex justify-between items-center border-b border-gray-200 py-2"></div>
        </div>
    );
}