import {useState, useEffect} from "react";

export const Dashboard = () => {
    // Récupération de la liste
    let [todoList, setTodoList] = useState(JSON.parse(localStorage.getItem("todoList")) || []);

    // trier la todolist du plus proche au plus loin
    setTodoList = todoList.sort((a, b) => {
        return new Date(a.deadline) - new Date(b.deadline);
    });

    // Todolist + nombre de tâche
    const todoListLength = todoList?.length || 0;

    // Récupération et conversion en pourcentage du nombre de tâches complétées
    const todoListCompleted = todoList?.filter((task) => task.is_completed === true).length || 0;
    const todoListCompletedPercentage = Math.round((todoListCompleted / todoListLength) * 100);

    // Récupération et conversion en pourcentage du nombre de tâches expirées
    const todoListPassed = todoList.filter((task) => task.is_passed === true).length;

    return (
        <div className="bg-white">
            <div className="bg-gray-50 pt-12 sm:pt-16">
                <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                    <div className="mx-auto max-w-4xl text-center">
                        <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Tableau de bord</h2>
                        <p className="mt-3 text-xl text-gray-500 sm:mt-4">Retrouvez ici toutes les statistiques et la liste
                        de vos tâches par ordre chronologique.</p>
                    </div>
                </div>
                <div className="mt-10 bg-white pb-12 sm:pb-16">
                    <div className="relative">
                        <div className="absolute inset-0 h-1/2 bg-gray-50"></div>
                        <div className="relative mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                            <div className="mx-auto max-w-4xl">
                                <dl className="rounded-lg bg-white shadow-lg sm:grid sm:grid-cols-3">
                                    <div
                                        className="flex flex-col border-b border-gray-100 p-6 text-center sm:border-0 sm:border-r">
                                        <dt className="order-2 mt-2 text-lg font-medium leading-6 text-gray-500">Total de tâche(s)</dt>
                                        <dd className="order-1 text-5xl font-bold tracking-tight text-indigo-600">{ todoListLength }</dd>
                                    </div>
                                    <div
                                        className="flex flex-col border-t border-b border-gray-100 p-6 text-center sm:border-0 sm:border-l sm:border-r">
                                        <dt className="order-2 mt-2 text-lg font-medium leading-6 text-gray-500">Tâche(s) complétée(s)</dt>
                                        <dd className="order-1 text-5xl font-bold tracking-tight text-indigo-600">{ todoListCompletedPercentage } %</dd>
                                    </div>
                                    <div
                                        className="flex flex-col border-t border-gray-100 p-6 text-center sm:border-0 sm:border-l">
                                        <dt className="order-2 mt-2 text-lg font-medium leading-6 text-gray-500">Tâche(s) expirée(s)</dt>
                                        <dd className="order-1 text-5xl font-bold tracking-tight text-indigo-600">{ todoListPassed }</dd>
                                    </div>
                                </dl>
                            </div>
                        </div>
                        <div className="mt-10 relative mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                            <div className="mx-auto max-w-2xl">
                                <dl className="px-6 py-6 rounded-lg bg-white shadow-lg">
                                    <p className="text-xl text-gray-700">Liste des tâches</p>
                                    <ul>
                                        {
                                            todoList.map((task, index) => (
                                                <li className={"pt-6 items-center mt-4 relative flex cursor-pointer rounded-lg ring-2 ring-indigo-500 bg-white p-4 shadow-sm focus:outline-none " + (task.is_passed ? 'border-red-500 ring-red-500' : 'border-indigo-500 ring-indigo-500')} key={ index }>
                                                    <div className="w-full mr-2 relative rounded-md border border-gray-300 px-3 py-2 shadow-sm focus-within:border-indigo-600 focus-within:ring-1 focus-within:ring-indigo-600">
                                                        <label htmlFor="taskName"
                                                               className="absolute -top-2 left-2 -mt-px inline-block bg-white px-1 text-xs font-medium text-gray-900">Tâche à remplir :</label>
                                                        <input type="text" name="taskName"
                                                               className="taskName block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                                                               defaultValue={ task.name } disabled />
                                                    </div>
                                                    <div className="w-full mr-2 relative rounded-md border border-gray-300 px-3 py-2 shadow-sm focus-within:border-indigo-600 focus-within:ring-1 focus-within:ring-indigo-600">
                                                        <label htmlFor="deadline"
                                                               className="absolute -top-2 left-2 -mt-px inline-block bg-white px-1 text-xs font-medium text-gray-900">Avant le :</label>
                                                        <input type="date" name="deadline"
                                                               className="taskDeadline block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                                                               defaultValue={ task.deadline } disabled />
                                                    </div>
                                                </li>
                                            ))
                                        }
                                    </ul>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}