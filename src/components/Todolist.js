import '../App.css';

export const TodoList = (props) => {
    // Suppression d'une tâche
    const deleteTask = (index) => {
        const newTaskList = props.todoList.filter((task, i) => i !== index);
        props.setTodoList(newTaskList);
    }

    // Conversion de l'item en formulaire de modification
    const switchToForm = (index) => {
        const editButton = document.querySelectorAll('.editButton')[index];
        const saveButton = document.querySelectorAll('.saveButton')[index];
        const taskName = document.querySelectorAll('.taskName')[index];
        const taskDeadline = document.querySelectorAll('.taskDeadline')[index];
        editButton.style.display = 'none';
        saveButton.style.display = 'inline';
        taskName.removeAttribute('disabled');
        taskDeadline.removeAttribute('disabled');
    }

    // Modification d'une tâche
    const editTask = (index) => {
        const editButton = document.querySelectorAll('.editButton')[index];
        const saveButton = document.querySelectorAll('.saveButton')[index];
        const taskName = document.querySelectorAll('.taskName')[index];
        const taskDeadline = document.querySelectorAll('.taskDeadline')[index];

        const newTaskList = props.todoList.map((task, i) => {
            if (i === index) {
                task.name = taskName.value;
                task.deadline = taskDeadline.value;
            }
            return task;
        });

        taskName.setAttribute('disabled', true);
        taskDeadline.setAttribute('disabled', true);
        saveButton.style.display = 'none';
        editButton.style.display = 'inline';

        props.setTodoList(newTaskList);
    }

    // Changer le statut d'une tâche
    const completeTask = (index) => {
        const newTaskList = props.todoList.map((task, i) => {
            if (i === index) {
                task.is_completed = !task.is_completed;
            }
            return task;
        });

        props.setTodoList(newTaskList);
    }

    return (
        <div className="mb-10">
            <ul>
                {
                    props.todoList.map((task, index) => (
                        <li className={"pt-6 items-center mt-4 relative flex cursor-pointer rounded-lg ring-2 ring-indigo-500 bg-white p-4 shadow-sm focus:outline-none " + (task.is_passed ? 'border-red-500 ring-red-500' : 'border-indigo-500 ring-indigo-500')} key={ index }>
                            <input className="h-6 w-6 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500 mr-2" type="checkbox" checked={ task.is_completed } onChange={ () => completeTask(index) }/>
                            <div className="w-full mr-2 relative rounded-md border border-gray-300 px-3 py-2 shadow-sm focus-within:border-indigo-600 focus-within:ring-1 focus-within:ring-indigo-600">
                                <label htmlFor="taskName"
                                       className="absolute -top-2 left-2 -mt-px inline-block bg-white px-1 text-xs font-medium text-gray-900">Tâche à remplir :</label>
                                <input type="text" name="taskName"
                                       className="taskName block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                                       defaultValue={ task.name } disabled />
                            </div>
                            <div className="w-full mr-2 relative rounded-md border border-gray-300 px-3 py-2 shadow-sm focus-within:border-indigo-600 focus-within:ring-1 focus-within:ring-indigo-600">
                                <label htmlFor="deadline"
                                       className="absolute -top-2 left-2 -mt-px inline-block bg-white px-1 text-xs font-medium text-gray-900">Avant le :</label>
                                <input type="date" name="deadline"
                                       className="taskDeadline block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm"
                                       defaultValue={ task.deadline } disabled />
                            </div>
                            <button
                                className="saveButton ml-2 inline-flex items-center rounded-md border border-transparent bg-indigo-100 px-3 py-2 text-sm font-medium leading-4 text-indigo-700 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                                onClick={ () => editTask(index) }>Sauvegarder</button>
                            <button
                                className="editButton ml-2 inline-flex items-center rounded-md border border-transparent bg-indigo-100 px-3 py-2 text-sm font-medium leading-4 text-indigo-700 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                                onClick={ () => switchToForm(index) }>Modifier</button>
                            <button className="ml-2 inline-flex items-center rounded-md border border-transparent bg-indigo-600 px-3 py-2 text-sm font-medium leading-4 text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                                onClick={ () => deleteTask(index) }>Supprimer</button>
                        </li>
                    ))
                }
            </ul>
        </div>
    );
}