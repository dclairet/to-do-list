export const TodoSearch = (props) => {
    // Recherche d'une tâche
    const searchTask = (e) => {
        const search = e.target.value;
        console.log(search)
        const newTaskList = props.todoList.filter((task) => {
            return task.name.toLowerCase().includes(search.toLowerCase());
        });
    }

    return (
        <div className="relative">
            <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
                <svg aria-hidden="true" className="w-5 h-5 text-gray-500" fill="none"
                     stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path strokeLinecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                </svg>
            </div>
            <input type="search" id="default-search"
                   className="block p-4 pl-10 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-indigo-500 focus:border-indigo-500"
                   onChange={searchTask}
                   placeholder="Rechercher une tâche..." />
        </div>
    );
}