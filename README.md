# ToDoList
Damien CLAIRET - M2 I2L Alternance.

## Lancer le projet

### `npm start`

Lien du projet [http://localhost:3000](http://localhost:3000)

## Page Tableau de bord
Cette page remonte quelques informations sur la liste des tâches et la liste des tâches triée par ordre chronologique.

![Tableau de bord](./public/dashboard.png "Tableau de bord")

## Page To Do List

Cette page permet de créer une nouvelle tâche, de la modifier, de la supprimer et de la marquer comme terminée.

Lorsque la deadline est passée, la tâche est marquée en rouge.

![Liste des tâches](./public/todolist.png "Liste des tâches")